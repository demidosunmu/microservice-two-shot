# Wardrobify

Team:

Connor Gariepy - Shoes
Demi Dosunmu - Hats

## Design

## Shoes microservice

To start with my microservice I first made the models of the the
Shoe and BinVO, then got to work on getting the poller working so
that I could make sure that React was able to properly fetch the
data from the /api/bins/ for the BinVO.

From there I created two views, one meant to "GET" the list of shoes
or "POST" to create a new one entirely, and the other who's only
purpose was to "DELETE" a specific shoe with a matching id.

After that I added "<ShoesForm>" and "<ShoesList>" to the App.js and
began to work on those two, starting with the ShoesList to first
display any shoes it fetched from the /api/shoes/ and placing them on
the webpage.  Then I worked on the ShoesForm which obtained the bin
information from the /api/bins/ in order to help a user decide which
bin to place the new pair of shoes into, making sure it accuaretely
updated the new shoe and its bin location.

## Hats microservice

I first started by creating the "Hats" and LocationVO models, the latter pulling from the wardrobe project.
I then created two functions: one to list location data, and the other to GET, POST, or DElETE data about the hats. I was unable to add data to these sets, and so I started working on the poller, so i could properly fetch the location data for my location value object.

After that, it was time to start workign on the FrontEnd.
I created a HatsList file for listing all the hat data, and a HatsForm file where an end user can create (POST) a new Hat. The data for these functions is fetched from /api/locations. I implemented a delete function in the ShoesList File which removes a shoe based on it's assigned Id.
