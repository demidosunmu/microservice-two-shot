from django.db import models
from django.urls import reverse

# Create your models here.


class BinVO(models.Model):
    """
    Represents a bin that may contain one or several shoes.

    BinVO is a value object so it does not have its own direct URL,
    import_href exists to point towards the original Bin.
    """

    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

    def get_api_url(self):
        return reverse("api_bin", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"


class Shoe(models.Model):
    """
    The Shoe model represents a shoe with its given manufacturer,
    modelname, color,included with a url to show the shoe in question,
    and a bin foreign key to reference the bin thatit belongs to.
    """

    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.model_name
