from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe

# Create your views here.


# class BinVOEncoder(ModelEncoder):
#    model = BinVO
#    properties = [
#        "import_href",
#        "closet_name",
#        "bin_number",
#        "bin_size",
#        ]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.id}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    """
    Lists the names of all of the shoes.

    Returns a dictionary with a single key called "shoes" which contains a
    list of shoes and their respective bin location.  Each entry in this
    dictionary contains all the information on each shoe, when called as a
    "POST" it will instead create a new shoe with that same template.

    {
        "shoes": [
            {
                "manufacturer": Manufacturer name,
                "model_name": Model name,
                "color": Color,
                "picture_url": Picture Url,
                "id": The shoe's unique ID,
                "bin": The bin ID,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(id=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_shoe(request, id):
    """
    When called with a specific shoe's ID this view will verify if the shoe ID
    is valid, and if so it will delete the shoe in question.
    """
    try:
        shoe = Shoe.objects.get(id=id)
    except Shoe.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid shoe ID"},
            status=400,
        )

    if request.method == "DELETE":
        shoe.delete()
        return JsonResponse(
            {"deleted": "This shoe has been deleted"}
        )
