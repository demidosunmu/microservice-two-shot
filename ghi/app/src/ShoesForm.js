import React, { useEffect, useState } from 'react';

function ShoesForm() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [pictureUrl, setPicture] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [bin, setBin] = useState('');

    // Handles for the various useState's
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    // Fetches data from the /api/bins/ to populate the page
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Applies the information of the form into the data dictionary
        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // If response.ok, creates a new shoe and resets the form
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setPicture('');
            setModelName('');
            setColor('');
            setBin('');
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe!</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={manufacturer} />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} placeholder="Picture Url" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" value={pictureUrl} />
                            <label htmlFor="pictureUrl">Picture Url</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} placeholder="Model Name" required type="text" name="modelName" id="modelName" className="form-control" value={modelName} />
                            <label htmlFor="modelName">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} required id="bin" name="bin" className="form-select" value={bin}>
                            <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.id}>
                                        {bin.closet_name} - Bin #{bin.bin_number}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create shoe!</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoesForm;
