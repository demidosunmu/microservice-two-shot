import React, { useEffect, useState } from 'react'

const HatList = () => {
  const [hats, setHats] = useState([]);

  const fetchData = async ()=> {
    const url = 'http://localhost:8090/api/hats/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  };

  const deleteHat = async (event, id) => {
    event.preventDefault();
    const url = `http://localhost:8090/api/hats/${id}`;
    const fetchConfig = {
        method: "delete",
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setHats(hats.filter((hat) => hat.id !== id));
    }
  };

  useEffect(() => {
    fetchData();
  }, [])

  return (
    <div className="container">
      <table className="table table-striped">
          <thead>
          <tr>
              <th>Fabric</th>
              <th>Style name</th>
              <th>Color</th>
          </tr>
          </thead>
          <tbody>
          {hats.map(hat => {
              return (
              <tr key={hat.id} value={hat.id} >
                  <td>{hat.fabric}</td>
                  <td>{hat.style_name}</td>
                  <td>{hat.color}</td>
                  <td><img src={hat.picture_url} style={{ width:"200px" }} /></td>
                  <td>
                    <button className="btn btn-lg btn-outline-primary" onClick={(event) => deleteHat(event, hat.id)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                    </svg></button>
                  </td>
              </tr>
              );
          })}
          </tbody>
      </table>
    </div>
  )
}

export default HatList
