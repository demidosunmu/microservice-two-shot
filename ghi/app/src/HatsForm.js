import React, { useEffect, useState } from 'react';

function HatsForm() {
    const[locations, setLocations] = useState([]);
    const[fabric, setFabric] = useState('');
    const[style_name, setStyleName] = useState('');
    const[color, setColor] = useState('');
    const[pictureUrl, setPictureUrl] = useState('');
    const[location, setLocation] = useState('');


    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value)
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }
    const handlepictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value)
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setLocations(data.locations)
        }

    }
    const handleSubmit = async (event) =>{
        event.preventDefault();

        const data = {};
        data.fabric = fabric;
        data.style_name = style_name;
        data.color= color;
        data.picture_url=pictureUrl;
        data.location = location;
        console.log(data);

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok){
        const newHat = await response.json();
        console.log(newHat)
        setFabric('');
        setColor('');
        setStyleName('');
        setPictureUrl('');
        setLocation('');
    }
}


useEffect(()=> {
    fetchData();
}, [])



return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat!</h1>
            <form onSubmit={handleSubmit}id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange= {handleFabricChange} placeholder="Fabric" required type="text" name="Fabric" id="fabric" className="form-control" value={fabric}/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange= {handleColorChange} placeholder="Color"  required type="text" name="Color" id="Color" className="form-control" value={color}/>
                <label htmlFor="Color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleNameChange} placeholder="Style Name"  required type="text" name="style_name" id="style_name" className="form-control" value={style_name}/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlepictureUrlChange} placeholder="Picture URL" required type="numbertext" name="picture_url" id="pictureUrl" className="form-control" value={pictureUrl}/>
                <label htmlFor="pictureUrl">Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={location}>
                  <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.href}>
                            {location.closet_name} Section Number: {location.section_number} Shelf Number: {location.shelf_number}
                            </option>

                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default HatsForm
