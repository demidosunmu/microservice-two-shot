import React, { useEffect, useState } from 'react';


function ShoesList() {
    const [shoes, setShoes] = useState([]);
    const [bins, setBins] = useState([]);
    const [shoeId, setShoeId] = useState('');

    const handleShoeId = async (event) => {
        const value = event.target.value;
        setShoeId(value);
    }

    const handleShoeDelete = async (event) => {
        event.preventDefault();

        const shoe = shoeId;
        const url = `http://localhost:8080/api/shoes/${shoe}/`;
        const fetchConfig = {
            method: "delete",
            body: JSON.stringify(shoe),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const deleteShoe = await response.json();
            console.log("Shoe with an ID of", shoeId, "has been deleted.");
            fetchData();
            setShoeId('');
        }
    }

    const fetchData = async () => {
        // Declaring two Urls to fetch from and placing them in an array
        // where they are called in order of shoes > bin
        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const binUrl = 'http://localhost:8100/api/bins/';

        const requests = [];
        requests.push(fetch(shoeUrl));
        requests.push(fetch(binUrl));

        // Waits for all requests to finish at the same time
        const responses = await Promise.all(requests);

        // Loops over the responses array, calling the
        // shoes first and setting them, and then the bins
        let count = 0;
        for (const response of responses) {
            if (response.ok) {
                const data = await response.json();
                if (count === 0) {
                    setShoes(data.shoes);
                    count = 1;
                } else {
                    setBins(data.bins);
                    count = 0;
                }
            }
        }

    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Manufacturer</th>
                        <th>Image</th>
                        <th>Bin</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={ shoe.id }>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td>{ shoe.manufacturer }</td>
                            <td><img src={shoe.picture_url} style={{height: 75, width: 75}}/></td>
                            {bins.map(bin => {
                                if (shoe.bin === bin.id) {
                                    return (
                                        <td key={bin.id}>Located in bin #{bin.bin_number} inside {bin.closet_name}</td>
                                    );
                                }
                            })}
                            <td>
                                <form onSubmit={handleShoeDelete}>
                                    <button onClick={handleShoeId} className="btn btn-danger" value={shoe.id}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                    </svg> Delete</button>
                                </form>
                            </td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </>
    );
}

export default ShoesList;
