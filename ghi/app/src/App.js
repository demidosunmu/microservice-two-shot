import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';
import HatsForm from './HatsForm';
import HatsList from './HatsList';





function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
          </Route>
          <Route path="hats/new" element={<HatsForm />} />
          <Route path="hats" element={<HatsList hats={props.hats} />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
